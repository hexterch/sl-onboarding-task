require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  render_views

  describe 'GET count' do
    context 'when there are multiple users existed' do
      it 'returns users count' do
	count = User.count
        get :count
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)['count']).to eq(count)
      end
    end
  end

  describe 'GET name' do
    context 'when there is a user existed' do
      let(:user) { create(:user) }

      it "returns user's name when user id is corrected" do
        get :name, id: user.id
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)['name']).to eq(user.name)
      end

      it "returns bad request when user id is not existed" do
        get :name, id: 999
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'GET index' do
    context 'when there is a user existed' do
      let!(:user) { create(:user) } 
      it "returns user's data" do
        get :index, format: :json
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status(:ok)
        expect(json_response[0]['first_name']).to eq(user.first_name)
      end
    end
  end
end
