FactoryBot.define do
  factory :user do
    first_name { Faker::Name.name }
    last_name { Faker::Name.name }
    age 18
    gender 'male' 
    address {}
  end
end
