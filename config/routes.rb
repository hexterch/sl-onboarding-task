Rails.application.routes.draw do
  resources :users do
    collection do
      get :count
    end
    member do
      get :name
    end
    resource :shop
  end
end
