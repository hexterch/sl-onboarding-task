describe('Users action', () => {
  var usersCount, users;
  beforeEach(() => {
    browser.get('http://54.169.123.170:32828/users');
    users = element.all(by.repeater('user in users'));
    users.count().then(function(usersIndexCount){
      usersCount = usersIndexCount;
    });
  });

  it('create a new user',() => {
    browser.get('http://54.169.123.170:32828/users/new');
    element(by.model('user.first_name')).sendKeys('test_first_name');
    element(by.model('user.last_name')).sendKeys('test_last_name2');
    element(by.model('user.age')).sendKeys('6969');
    element(by.model('user.gender')).sendKeys('male');
    element(by.css('[type="submit"]')).click();
    browser.get('http://54.169.123.170:32828/users');
    expect(users.count()).toEqual(usersCount+1);
  });

  it('updates a existed user',() => {
    browser.get('http://54.169.123.170:32828/users/5dc551e0c1d36145f9000002/edit');
    element(by.model('user.age')).clear().sendKeys('999');
    element(by.css('[type="submit"]')).click();
    browser.get('http://54.169.123.170:32828/users/5dc551e0c1d36145f9000002');
    expect(element(by.id('age')).getText()).toEqual('999');
  });

  it('detroys a user',() => {
    element.all(by.css('[class="remove_user"]')).get(-1).click();
    browser.get('http://54.169.123.170:32828/users');
    expect(users.count()).toEqual(usersCount - 1);
  });
});
