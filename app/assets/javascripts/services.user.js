myApp.service('userService', [
  '$http',
  function($http) {
    this.getUser = function(id) {
      return $http({
        method: 'GET',
        url: '/users/' + id + '.json'
      });
    };

    this.getUsers = function() {
      return $http({
        method: 'GET',
        url: '/users.json'
      });
    };

    this.createUser = function(params) {
      return $http({
        method: 'POST',
        url: '/users',
        data: params
      });
    };

    this.updateUser = function(id, params) {
      return $http({
        method: 'PUT',
        url: '/users/' + id + '.json',
        data: params
      });
    };

    this.deleteUser = function(id) {
      return $http({
        method: 'DELETE',
        url: '/users/' + id   
      });
    };
  }
]);
