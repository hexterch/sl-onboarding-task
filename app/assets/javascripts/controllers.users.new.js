myApp.controller('UserNewController', [
  '$scope',
  function UserNewController($scope) {
    $scope.init = function(action){
      $scope.action = action;
      $scope.user = { first_name: '', last_name: '', age: 0 };
    }
  }
]);
