myApp.directive('userForm', [
  '$window',
  'userService',
  function($window, userService) {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: '/users/_form.html',
      link: function(scope, element, attrs) {
        scope.submit = function (){
          switch(scope.action) {
            case 'edit':
              params = { user: scope.user }
              var promise = userService.updateUser(attrs.id, params);
              promise.then(
                function(response) {
                  $window.location.href = '/users/' + attrs.id;
                },
                function(error) {
                  $window.location.href = '/users/' + attrs.id + '/edit';
                }
              );
              break;
            case 'create':
              params = { user: scope.user }
              var promise = userService.createUser(params);
              promise.then(
                function(response) {
                  $window.location.href = '/users';
                },
                function(error) {
                  $window.location.href = '/users/' + attrs.id + '/edit';
                }
              );
              break;
          }
        }
      }
    };
  }
]);
