myApp.controller('UserEditContoller', [
  '$scope',
  'userService',
  function UserEditController($scope, userService) {
    $scope.init = function(id, action){
      $scope.action = action;
      getUser(id);
    }
    function getUser(id) {
      var promise = userService.getUser(id);
      promise.then(
        function(response) {
          $scope.user = response.data;
        }
      );
    }
  }
]);
