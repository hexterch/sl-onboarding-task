myApp.controller('UsersController', [
  'userService',
  '$window',
  '$scope',
  function UsersController(userService, $window, $scope) {
    $scope.users = [];

    var promise = userService.getUsers();
    promise.then(function(response) {
      $scope.users = response.data;
    });

    $scope.displayEditForm = function(user) {
      $scope.show = user._id.$oid;
      $scope.action = 'edit';
    };

    $scope.deleteUser = function(id) {
      var promise = userService.deleteUser(id);
      promise.then(
        function(response) {
          $window.location.href = '/users'
        }
      );
    }
  }
]);
