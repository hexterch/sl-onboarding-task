class User
  include Mongoid::Document
  include Mongoid::Enum

  has_one :shop

  field :first_name, type: String
  field :last_name, type: String
  field :age, type: Integer
  field :gender, type: String
  field :address, type: Hash, default: {}
 

  validates_presence_of :first_name, :last_name, :age, :gender
  validates_numericality_of :age, :greater_than => 0

  enum :genders, [:male, :female, :others]

  def name
    first_name + ' ' + last_name
  end
end
