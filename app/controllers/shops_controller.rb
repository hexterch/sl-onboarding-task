class ShopsController < ApplicationController
  before_action :set_user
  before_action :set_shop, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    @shop = Shop.new
  end

  def edit
  end

  def create
    @shop = Shop.new(shop_params)
    @shop.user_id = @user.id
    respond_to do |format|
      if @shop.save
        format.html { redirect_to @user, notice: "#{shop_params}" }
        format.json { render :show, status: :created, location: @shop }
      else
        format.html { render :new }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @shop.update(shop_params)
        format.html { redirect_to @user, notice: 'Shop was successfully updated.' }
        format.json { render :show, status: :ok, location: @shop }
      else
        format.html { render :edit }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @shop.destroy
    respond_to do |format|
      format.html { redirect_to @user, notice: 'Shop was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_shop
      @shop = @user.shop
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def shop_params
      params.require(:shop).permit(:name)
    end
end
