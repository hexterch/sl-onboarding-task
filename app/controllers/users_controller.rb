class UsersController < ApplicationController
  rescue_from Mongoid::Errors::DocumentNotFound, with: :render_not_found
  before_action :set_user, only: [:show, :edit, :update, :destroy, :name]

  def index
    @users = User.all
  end

  def show
    respond_to do |format|
      format.html { render :show }
      format.json { render json: @user.to_json, status: :ok }
    end
  end

  def new
    @user = User.new
  end

  def edit
    respond_to do |format|
      format.html { render :edit }
      format.json { render json: @user.to_json, status: :ok }
    end
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render nothing: true, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    head :no_content
  end

  def count
    @count = User.count

    render json: { count: @count }
  end

  def name
    render json: { name: @user.name }
  end

  def index
    @users = User.all
    respond_to do |format|
      format.html { render :index }
      format.json { render json: @users.to_json, status: :ok }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def render_not_found
    render json: { message: 'resource not found' }, status: :not_found
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :age, :gender, address: [:country, :address_1, :address_2])
  end
end
