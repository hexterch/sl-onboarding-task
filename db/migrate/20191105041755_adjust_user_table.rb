class AdjustUserTable < ActiveRecord::Migration
  def up
    change_column :users, :first_name, :string, null: false
    change_column :users, :last_name, :string, null: false
    add_column :users, :age, :int
    add_column :users, :gender, :int
  end

  def down
    change_column :users, :first_name, :string, null: true
    change_column :users, :last_name, :string, null: true
    remove_column :users, :age
    remove_column :users, :gender
  end
end
